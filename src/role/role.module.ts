import { Module, forwardRef } from '@nestjs/common';
import { RoleService } from './role.service';
import { RoleController } from './role.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Role } from './entity/role.entity';
import { CommonModule } from 'src/common/common.module';
import { PermissionModule } from 'src/permission/permission.module';
import { Permission, RolePermission } from 'src/permission/entity/permission.entity';

@Module({
  imports: [SequelizeModule.forFeature([Role, RolePermission, Permission]), CommonModule, forwardRef(() => PermissionModule)],
  providers: [RoleService],
  controllers: [RoleController],
  exports: [RoleService]
})
export class RoleModule { }
