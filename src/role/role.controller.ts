import { Body, Controller, HttpStatus, Post, Get, Param, Put, Req, Res } from '@nestjs/common';
import { RoleService } from './role.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { Request, Response } from 'express';
import { AssignBatchPermissionDto, AssignPermissionDto, UpdateRoleDto } from './dto/update-role.dto';
import { CommonService } from 'src/common/common.service';

@Controller('role')
export class RoleController {
    constructor(
        private roleService: RoleService,
        private commonService: CommonService
    ) { }

    @Post()
    async create(@Body() createRoleDto: CreateRoleDto, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const orgAdmin = await this.commonService.checkOrganizationAdmin(auth);
        if (!orgAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

        const alias = this.commonService.toNoSpaceLowerCase(createRoleDto.name);
        const organizationId = auth.userInfo.organizationId
        const isExists = await this.roleService.findOne({ alias, organizationId })
        if (isExists.success) return response.status(HttpStatus.BAD_REQUEST).send("Already exists")

        createRoleDto.alias = alias;
        createRoleDto.organizationId = organizationId
        const serviceResponse = await this.roleService.create(createRoleDto);
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Post("assign-permission")
    async assignPermission(@Body() assignPermissionDto: AssignPermissionDto, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const orgAdmin = await this.commonService.checkOrganizationAdmin(auth)
        if (!orgAdmin || assignPermissionDto.organizationId !== auth.userInfo.organizationId) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

        const serviceResponse = await this.roleService.assignPermission(assignPermissionDto);
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Post("assign-batch-permission")
    async assignBatchPermission(@Body() assignPermissionDto: AssignBatchPermissionDto, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const orgAdmin = await this.commonService.checkOrganizationAdmin(auth)
        if (!orgAdmin || assignPermissionDto.organizationId !== auth.userInfo.organizationId) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

        const serviceResponse = await this.roleService.assignBatchPermission(assignPermissionDto);
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Get()
    async findAll(@Req() request: Request, @Res() response: Response)  {

        const auth = request.headers.data as any;
        const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName)
        if (!superAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

        const serviceResponse = await this.roleService.findAll({});
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Get(":id")
    async findOne(@Param('id') id: string, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName)
        const orgAdmin = await this.commonService.checkOrganizationAdmin(auth)
        if (!(superAdmin || orgAdmin)) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

        const serviceResponse = await this.roleService.findOne({ id });
        if(!superAdmin && serviceResponse?.data?.organizationId !== auth.userInfo.organizationId) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized");

        if (!superAdmin && orgAdmin && serviceResponse?.data?.organizationId !== auth.userInfo.organizationId) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Get("user/organization")
    async findAllOfUser(@Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const orgAdmin = await this.commonService.checkOrganizationAdmin(auth)
        if (!orgAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

        const serviceResponse = await this.roleService.findAll({ organizationId: auth.userInfo.organizationId });
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Get("organization/:orgId")
    async findAllByOrg(@Param('orgId') organizationId: string, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName)
        if (!superAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

        const serviceResponse = await this.roleService.findAll({ organizationId });
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Get("permission/:permissionId")
    async findAllofRole(@Param('permissionId') id: string, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const orgAdmin = await this.commonService.checkOrganizationAdmin(auth);
        if (!orgAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized");

        const isExists = await this.roleService.findOnePermission({ id })
        if(isExists?.data?.organizationId !== auth?.userInfo?.organizationId) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized");

        const role = await this.roleService.findByPermission(id);
        if (!role.data) return response.status(HttpStatus.BAD_REQUEST).send("Role not found");
        if (orgAdmin && role.data.organizationId !== auth.userInfo.organizationId) return response.status(HttpStatus.BAD_REQUEST).send("Unauthorized role");

        const serviceResponse = await this.roleService.findByPermission(id);
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Put(":id")
    async update(@Param('id') id: string, @Body() updateDto: CreateRoleDto, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const orgAdmin = await this.commonService.checkOrganizationAdmin(auth);
        if (!orgAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized");

        let alias
        if (updateDto.name) alias = this.commonService.toNoSpaceLowerCase(updateDto.name);
        if (updateDto.organizationId) delete updateDto.organizationId

        const organizationId = auth.userInfo.organizationId
        
        const roleDetails = await this.roleService.findOne({ id })
        const isExists = await this.roleService.findOne({ organizationId, alias })

        if(roleDetails?.data?.organizationId !== auth.userInfo.organizationId) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized");
        if (isExists.success) return response.status(HttpStatus.BAD_REQUEST).send("Already exists")
        if (alias == 'general-employee') return response.status(HttpStatus.BAD_REQUEST).send("You can't change this!")

        updateDto.alias = alias;
        const serviceResponse = await this.roleService.update(id, updateDto);
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

}
