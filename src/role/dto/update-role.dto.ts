import { PartialType } from '@nestjs/mapped-types';
import { CreateRoleDto } from './create-role.dto';

export class UpdateRoleDto extends PartialType(CreateRoleDto) { }

export class AssignBatchPermissionDto {
    id: string;
    permissions: string[];
    organizationId: string;
}
export class AssignPermissionDto {
    id: string;
    permissionId: string;
    organizationId: string;
}
