import { BelongsTo, BelongsToMany, Column, DataType, ForeignKey, Index, Model, PrimaryKey, Table, Unique } from 'sequelize-typescript';
import { Organization } from 'src/organization/entities/organization.entity';
import { Permission, RolePermission } from 'src/permission/entity/permission.entity';

@Table
export class Role extends Model {


    @PrimaryKey
    @Column({ primaryKey: true, type: DataType.UUID, defaultValue: DataType.UUIDV4 })
    id: number

    @Column({ allowNull: false })
    name: string;

    @Column({ allowNull: false })
    alias: string;

    @Column({ allowNull: false })
    description: string;

    @Column({ defaultValue: true })
    isActive: boolean;

    @Index
    @ForeignKey(() => Organization)
    @Column({type: DataType.UUID})
    organizationId: string;

    @BelongsTo(() => Organization)
    organization: Organization;

    @BelongsToMany(()=> Permission, ()=> RolePermission)
    permissions: Permission;
}