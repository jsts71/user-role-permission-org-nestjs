import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Sequelize } from 'sequelize-typescript';
import { Role } from './entity/role.entity';
import { CommonService } from 'src/common/common.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { AssignBatchPermissionDto, AssignPermissionDto, UpdateRoleDto } from './dto/update-role.dto';
import { Permission, RolePermission } from 'src/permission/entity/permission.entity';
import { WhereOptions } from 'sequelize';

@Injectable()
export class RoleService {
    constructor(
        private sequelize: Sequelize,
        @InjectModel(Role)
        private roleModel: typeof Role,
        @InjectModel(RolePermission)
        private rolePermissionModel: typeof RolePermission,
        @InjectModel(Permission)
        private permissionModel: typeof Permission,
        private commonService: CommonService,
    ) { }

    async create(createRoleDto: CreateRoleDto) {
        try {
            return await this.sequelize.transaction(async t => {
                const transactionHost = { transaction: t };
                let result = await this.roleModel.create(
                    { ...createRoleDto },
                    transactionHost,
                );
                return this.commonService.successResponse(result.dataValues, "Successful!");
            })
        } catch (error) {
            return this.commonService.errorResponse("Failed!", error);
        }
    }

    async findOne(condition: WhereOptions<any>) {
        try {
            const role = await this.roleModel.findOne({
                where: condition,
            });
            return this.commonService.successResponse(role, "Successful!");
        } catch (error) {
            return this.commonService.errorResponse("Failed!", error);
        };

    }

    async findAll(condition: WhereOptions<any>) {
        try {
            const role = await this.roleModel.findAll({
                where: condition,
            });
            return this.commonService.successResponse(role, "Successful!");
        } catch (error) {
            return this.commonService.errorResponse("Failed!", error);
        };

    }
    

    async findOnePermission(condition: WhereOptions<any>) {
        try {
            const role = await this.roleModel.findOne({
                where: condition,
            });
            return this.commonService.successResponse(role, "Successful!");
        } catch (error) {
            return this.commonService.errorResponse("Failed!", error);
        };

    }

    async assignPermission(assignPermissionDto: AssignPermissionDto) {
        try {
            return await this.sequelize.transaction(async t => {
                const transactionHost = { transaction: t };

                // filter org permission only
                let permissionId = assignPermissionDto.permissionId;
                const permissionInfo = await this.permissionModel.findOne(
                    {
                        where: {
                            id: permissionId,
                        }
                    }
                );
                if (permissionInfo.organizationId !== assignPermissionDto.organizationId) {
                    return this.commonService.errorResponse("Not permitted!", "Permission error");
                }
                await this.rolePermissionModel.create(
                    { roleId: assignPermissionDto.id, permissionId: permissionId },
                    transactionHost,
                );

                return this.commonService.successResponse(assignPermissionDto, "Successful!");
            })
        } catch (error) {
            return this.commonService.errorResponse("Failed!", error);
        }
    }

    async assignBatchPermission(assignPermissionDto: AssignBatchPermissionDto) {
        try {
            return await this.sequelize.transaction(async t => {
                const transactionHost = { transaction: t };

                // filter org permission only
                let permissions = assignPermissionDto.permissions;
                const permissionInfo = await this.permissionModel.findAll(
                    {
                        where: {
                            id: permissions,
                        }
                    }
                );
                permissions = permissionInfo.filter(permission => permission.organizationId == assignPermissionDto.organizationId).map(permission => permission.id)
                console.log("permissions-------->", permissions)

                // execute assign
                // fetch rolePermissions mapping where role id is this role
                const rolePermissions = await this.rolePermissionModel.findAll(
                    {
                        where: {
                            roleId: assignPermissionDto.id,
                        }
                    }
                );

                // map only permissionId from these rolePermissions map
                const rolePermissionIds = rolePermissions.map((rolePermission) => rolePermission.permissionId)
                // filter the ids where id not in current assigned permission list
                const rolePermissionToDelete = rolePermissionIds.filter((id) => !permissions.includes(id))
                const rolePermissionToCreate = permissions.filter((id) => !rolePermissionIds.includes(id))

                for (const permission of rolePermissionToCreate) {
                    await this.rolePermissionModel.create(
                        { roleId: assignPermissionDto.id, permissionId: permission },
                        transactionHost,
                    );
                }

                for (const permission of rolePermissionToDelete) {
                    await this.rolePermissionModel.destroy({
                        where: {
                            roleId: assignPermissionDto.id,
                            permissionId: permission
                        }
                    })
                }

                assignPermissionDto.permissions = permissions
                return this.commonService.successResponse(assignPermissionDto, "Successful!");
            })
        } catch (error) {
            return this.commonService.errorResponse("Failed!", error);
        }
    }

    async findByPermission(permissionId: string) {
        try {
            const rolePermissions = await this.rolePermissionModel.findAll({ where: { permissionId } })
            const roleIds = rolePermissions.map((rolePermission) => rolePermission.roleId)
            const permission = await this.roleModel.findAll({
                where: {
                    id: roleIds
                },
            });
            return this.commonService.successResponse(permission, "Successful!");
        } catch (error) {
            return this.commonService.errorResponse("Failed!", error);
        };
    }
    
  async update(id: string, updateDto: CreateRoleDto) {
    try {
      const permission = await this.roleModel.update(updateDto, {
        where: {
          id: id
        },
      });
      return this.commonService.successResponse(permission, "Successful!");
    } catch (error) {
      return this.commonService.errorResponse("Failed!", error);
    };
  }
}
