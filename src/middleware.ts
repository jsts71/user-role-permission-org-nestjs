import { Injectable, NestMiddleware, HttpStatus } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import jwt_decode from "jwt-decode";
import { UserService } from './user/user.service';
import { PermissionService } from './permission/permission.service';
import { InjectModel } from '@nestjs/sequelize';
import { Permission, RolePermission } from './permission/entity/permission.entity';
import { CommonService } from './common/common.service';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  constructor(
    private commonService: CommonService,
    private permissionService: PermissionService,
    @InjectModel(Permission)
    private permissionModel: typeof Permission,
    @InjectModel(RolePermission)
    private rolePermissionModel: typeof RolePermission,
    private configService: ConfigService
  ) { }

  async use(req: Request, res: Response, next: NextFunction) {

    this.commonService.socket.emit('currentEvent', {
      evntOrigin: "URPO",
      time: new Date(),
      requestTo: req.url
    });

    let authorization = req.headers.authorization;
    const verified = await this.commonService.checkToken(authorization.split(" ")[1])

    if (!verified) return res.status(HttpStatus.UNAUTHORIZED).send({
      message: "Token expired",
      success: false,
      status: 403
    })

    const rolePermissions = await this.rolePermissionModel.findAll(
      {
        where: {
          roleId: verified?.roleId,
        }
      }
    );
    const rolePermissionIds = rolePermissions.map((rolePermission) => rolePermission.permissionId)

    const permissions = await this.permissionModel.findAll(
      {
        where: {
          id: rolePermissionIds,
        }
      }
    );
    const permissionDetails = permissions.map((permission) => ({ id: permission.id, name: permission.name, organization: permission.organizationId }))

    let data: any = {
      userInfo: verified,
      permissionDetails: permissionDetails
    }

    console.log(rolePermissionIds, permissionDetails, data, req.originalUrl )

    req.headers.data = data

    next();
  }
}
