import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/sequelize';
import * as bcrypt from 'bcrypt';
import * as dayjs from 'dayjs';
import { Sequelize } from 'sequelize-typescript';
import { Socket, io } from 'socket.io-client';
import { DefaultEventsMap } from 'socket.io/dist/typed-events';
import { Settings, SuperAdmin } from 'src/configuration/consts';
import { Organization } from 'src/organization/entities/organization.entity';
import { OrganizationService } from 'src/organization/organization.service';
import { Permission, RolePermission } from 'src/permission/entity/permission.entity';
import { Role } from 'src/role/entity/role.entity';
import { User } from 'src/user/entity/user.entity';

@Injectable()
export class CommonService {
    public socket: Socket<DefaultEventsMap, DefaultEventsMap>;

    constructor(
        private sequelize: Sequelize,
        @InjectModel(Organization)
        private organizationModel: typeof Organization,
        @InjectModel(Role)
        private roleModel: typeof Role,
        @InjectModel(Permission)
        private permissionModel: typeof Permission,
        @InjectModel(User)
        private userModel: typeof User,
        @InjectModel(RolePermission)
        private rolePermissionModel: typeof RolePermission,
        private configService: ConfigService,
        @Inject(forwardRef(() => OrganizationService))
        private organizationService: OrganizationService,
        private readonly jwtService: JwtService,
    ) {
        this.socket = io(this.configService.get('settings.socket'));
        this.socket.connect()
    }

    async adminEnvInfo(): Promise<SuperAdmin> {
        let supperAdmin = await this.configService.get<SuperAdmin>('superAdmin');
        return supperAdmin;
    }

    async settingsEnvInfo(): Promise<Settings> {
        let settings = await this.configService.get<Settings>('settings');
        return settings;
    }

    toNoSpaceLowerCase(context: string) {
        return context.replace(/\s+/g, '-').toLowerCase();
    }
    

    isPhone(contact: string) {
        return contact.match(/^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g);
    }

    async checkInit() {
        try {
            const superAdmin = await this.adminEnvInfo()
            const checkOrganization = await this.organizationService.findOne({ name: superAdmin.organizationName });
            return checkOrganization.success && checkOrganization.data;
        } catch (error) {
            throw error
        }
    }

    async initializeApplication() {
        try {
            console.info("Project setup Initiated!");
            const { organizationName, userName, password, permission, phone, email, firstName, lastName, role, employeePermission, employeeRole } = await this.adminEnvInfo();
            await this.sequelize.transaction(async t => {
                const transactionHost = { transaction: t };

                const organizationResult = await this.organizationModel.create(
                    { name: organizationName, description: "", alias: this.toNoSpaceLowerCase(organizationName), isAllowed: true },
                    transactionHost,
                );
                const organizationId = organizationResult.dataValues.id;

                const permissionResult = await this.permissionModel.create(
                    { name: permission, organizationId, description: "", alias: this.toNoSpaceLowerCase(permission) },
                    transactionHost,
                );
                const permissionId = permissionResult.dataValues.id;

                const empPermissionResult = await this.permissionModel.create(
                    { name: employeePermission, organizationId, description: "", alias: this.toNoSpaceLowerCase(employeePermission) },
                    transactionHost,
                );
                const empPermissionId = empPermissionResult.dataValues.id;

                const roleResult = await this.roleModel.create(
                    { name: role, organizationId, description: "", alias: this.toNoSpaceLowerCase(role) },
                    transactionHost,
                );
                const roleId = roleResult.dataValues.id;

                const empRoleResult = await this.roleModel.create(
                    { name: employeeRole, organizationId, description: "", alias: this.toNoSpaceLowerCase(employeeRole) },
                    transactionHost,
                );
                const empRoleId = empRoleResult.dataValues.id;

                await this.rolePermissionModel.create(
                    { roleId, permissionId },
                    transactionHost,
                )

                await this.rolePermissionModel.create(
                    { roleId: empRoleId, permissionId: empPermissionId },
                    transactionHost,
                )

                const userResult = await this.userModel.create({
                    firstName,
                    lastName,
                    userName,
                    password: await this.bcryptPassword(password),
                    phone,
                    email,
                    organizationId,
                    roleId,
                    phoneVerified: true,
                    emailVerified: true,
                    userAsOrg: true,
                    isActive: true,
                    isAllowed: true
                }, transactionHost);
                const userResultData = userResult.dataValues;
                console.info("Project setup completed!");
                console.info("User Id: ", userResultData.id, "\nUser Name: ", userResultData.userName, "\nPassword: ", userResultData.password)
            })
        } catch (error) {
            throw error
        }
    }

    async bcryptPassword(password: string) {
        const saltOrRounds = await this.configService.get('settings.saltOrRounds');
        return await bcrypt.hash(password, saltOrRounds);
    }

    async isPasswordMatch(password: string, hash: string) {
        return await bcrypt.compare(password, hash);
    }

    successResponse(data = null, message = "Successful", status = 200, success = true) {
        return { message, success, statusCode: status, data };
    };

    errorResponse(message = null, data = null, status = 400, success = false) {
        return { message, data, error: data, success, statusCode: status };
    };

    socketClient() {
        return this.socket;
    }

    async toPostGresTimeStamp(date: Date) {
        return dayjs(date).format('YYYY-MM-DD HH:mm:ss.SSSZZ')
    }

    async changeDateTime(date: Date, sign: '+' | '-', seconds?: number) {
        let life = seconds ?? (await this.settingsEnvInfo()).OTPLifeTime;
        if (sign === '-') life = life * (-1)
        const declared = new Date(date);
        let newLifeTime = new Date(declared.getTime() + 1000 * life);
        return newLifeTime
    }

    async isDateExpired(date: Date, seconds?: number) {
        const life = seconds ?? (await this.settingsEnvInfo()).OTPLifeTime;
        const declared = new Date(date);
        let lifeTime = new Date(declared.getTime() + 1000 * life);
        return new Date() > lifeTime
    }

    async checkTokenForReAuth(token: string) {
        try {
            const currentDateTime = new Date();
            const { jwtSecret: secret, privateKey, accessTokenLife, refreshTokenLife } = await this.settingsEnvInfo();
            let verified = await this.jwtService.verifyAsync(token, { secret });
            const tokenTime = new Date(verified.dateTime);
            let tokenRefreshTime = new Date(tokenTime.getTime() + 1000 * refreshTokenLife * (verified.rememberMe ? 48 : 1));
            if (tokenRefreshTime < currentDateTime) return false
            return verified;
        } catch (error) {
            return false
        }
    }

    async createToken(payload) {
        const { jwtSecret: secret, privateKey } = await this.settingsEnvInfo()
        let access_token = await this.jwtService.signAsync({ ...payload, dateTime: new Date() }, { secret, privateKey })
        let refresh_token = await this.jwtService.signAsync({ ...payload, dateTime: new Date() }, { secret, privateKey })
        return { access_token, refresh_token }
    }

    async checkToken(token: string) {
        try {
            const currentDateTime = new Date();
            const { jwtSecret: secret, privateKey, accessTokenLife, refreshTokenLife } = await this.settingsEnvInfo();
            let verified = await this.jwtService.verifyAsync(token, { secret });
            const tokenTime = new Date(verified.dateTime);
            let tokenAccessTime = new Date(tokenTime.getTime() + 1000 * accessTokenLife);
            let tokenRefreshTime = new Date(tokenTime);
            tokenRefreshTime.setSeconds(tokenTime.getSeconds() + refreshTokenLife);
            if (tokenAccessTime < currentDateTime) return false
            console.log(verified)
            return verified;
        } catch (error) {
            return false
        }
    }

    async checkSuperAdmin(userName: string) {
        try {
            const superAdmin = await this.adminEnvInfo()
            return superAdmin.userName === userName
        } catch (error) {
            return false
        }
    }

    async checkOrganizationAdmin(auth: { userInfo: any, permissionDetails: any[] }, permissionName?: string) {
        const { userInfo, permissionDetails } = auth
        try {
            const admin = userInfo.userAsOrg && userInfo.organizationId && userInfo.isAllowed && userInfo.isActive
            if (admin) return true
            const havePermission = permissionDetails.filter(permission => permission.name.toLowerCase() == permissionName.toLowerCase());
            if (havePermission.length) return true
            return false
        } catch (error) {
            return false
        }
    }
    

    async checkOrganizationUser(auth: { userInfo: any, permissionDetails: any[] }, permissionName?: string) {
        const { userInfo, permissionDetails } = auth
        try {
            const admin = userInfo.organizationId && userInfo.isAllowed && userInfo.isActive
            if (admin) return true
            const havePermission = permissionDetails.filter(permission => permission.name.toLowerCase() == permissionName.toLowerCase());
            if (havePermission.length) return true
            return false
        } catch (error) {
            return false
        }
    }

    async createOTP() {
        return '1234'
    }

    async sendPhoneOTP(phoneOTP: string, phone: string) {
        return true;
    }

    async sendEmailOTP(emailOTP: string, mailAddress: string) {
        return true;
    }
}
