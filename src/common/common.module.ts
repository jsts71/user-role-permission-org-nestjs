import { Module, forwardRef } from '@nestjs/common';
import { CommonService } from './common.service';
import { ConfigModule } from '@nestjs/config';
import { OrganizationModule } from 'src/organization/organization.module';
import { Organization } from 'src/organization/entities/organization.entity';
import { SequelizeModule } from '@nestjs/sequelize';
import { Role } from 'src/role/entity/role.entity';
import { Permission, RolePermission } from 'src/permission/entity/permission.entity';
import { User } from 'src/user/entity/user.entity';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [SequelizeModule.forFeature([Organization, Role, Permission, User, RolePermission]), forwardRef(() => OrganizationModule), ConfigModule, JwtModule],
  providers: [CommonService],
  exports: [CommonService]
})
export class CommonModule { }
