import { Controller, Get, Post, Body, Patch, Param, Delete, Req, Res, HttpStatus, Query } from '@nestjs/common';
import { OrganizationService } from './organization.service';
import { CreateOrganizationCodeDto, CreateOrganizationDto } from './dto/create-organization.dto';
import { UpdateOrganizationDto } from './dto/update-organization.dto';
import { Request, Response } from 'express';
import { CommonService } from 'src/common/common.service';
import { PaginationDTO } from 'src/configuration/consts';

@Controller('organization')
export class OrganizationController {
  constructor(
    private readonly organizationService: OrganizationService,
    private readonly commonService: CommonService
  ) { }

  @Post()
  async create(@Body() createOrganizationDto: CreateOrganizationDto, @Req() request: Request, @Res() response: Response) {

    const auth = request.headers.data as any;
    const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName)
    if (!superAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

    const serviceResponse = await this.organizationService.create(createOrganizationDto);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @Get()
  async findAllOrg(@Query() paginationDTO: PaginationDTO, @Req() request: Request, @Res() response: Response) {

    const auth = request.headers.data as any;
    const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName)
    if (!superAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

    const serviceResponse = await this.organizationService.findAndCOuntAll({}, paginationDTO);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @Get(":id")
  async findOne(@Param('id') id: string, @Req() request: Request, @Res() response: Response) {

    const auth = request.headers.data as any;
    const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName)
    if (!superAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

    const serviceResponse = await this.organizationService.findOne({ id });
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @Get("user/current")
  async find(@Req() request: Request, @Res() response: Response) {
    const auth = request.headers.data as any;
    const orgAdmin = await this.commonService.checkOrganizationUser(auth)
    if (!orgAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

    const serviceResponse = await this.organizationService.findOne({ id: auth.userInfo.organizationId });
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateOrganizationDto: UpdateOrganizationDto, @Req() request: Request, @Res() response: Response) {

    const auth = request.headers.data as any;
    const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName)
    const orgAdmin = await this.commonService.checkOrganizationAdmin(auth)
    if (!(superAdmin || (orgAdmin && auth.userInfo.organizationId == id))) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

    let alias
    if (updateOrganizationDto.name) alias = this.commonService.toNoSpaceLowerCase(updateOrganizationDto.name);

    const isExists = await this.organizationService.findOne({ alias })
    if (isExists.success) return response.status(HttpStatus.BAD_REQUEST).send("Already exists")

    updateOrganizationDto.alias = alias
    const serviceResponse = await this.organizationService.update(id, updateOrganizationDto);
    return serviceResponse.success ? response.send({ ...serviceResponse, data: updateOrganizationDto }) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @Post("create/org-code")
  async createOrganizationCode(@Body() createOrganizationCodeDto: CreateOrganizationCodeDto, @Req() request: Request, @Res() response: Response) {
    const auth = request.headers.data as any;
    const orgAdmin = await this.commonService.checkOrganizationAdmin(auth)
    if (!orgAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

    const serviceResponse = await this.organizationService.createOrganizationCode(auth, createOrganizationCodeDto);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @Get("get/org-code")
  async getOrganizationCodes(@Query() paginationDTO: PaginationDTO, @Req() request: Request, @Res() response: Response) {
    const auth = request.headers.data as any;
    const orgAdmin = await this.commonService.checkOrganizationAdmin(auth)
    if (!orgAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

    const serviceResponse = await this.organizationService.getOrganizationCodes(auth, paginationDTO);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.organizationService.remove(+id);
  }
}
