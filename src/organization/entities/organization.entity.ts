import { AutoIncrement, Column, DataType, Default, ForeignKey, HasMany, Index, IsUUID, Model, NotNull, PrimaryKey, Table, Unique } from 'sequelize-typescript';
import { Role } from 'src/role/entity/role.entity';
import { User } from 'src/user/entity/user.entity';

@Table
export class Organization extends Model {

    @PrimaryKey
    @Column({ primaryKey: true, type: DataType.UUID, defaultValue: DataType.UUIDV4 })
    id: string

    @Column({ allowNull: false })
    name: string;

    @Column({ allowNull: false })
    alias: string;

    // @Index
    // @Unique
    // @Column({ allowNull: false, type: DataType.UUID, defaultValue: DataType.UUIDV4 })
    // code: string;

    @Column({ allowNull: false })
    description: string;

    @Column({ defaultValue: true })
    isActive: boolean;

    @Column({ defaultValue: false })
    isAllowed: boolean;

    @HasMany(() => User)
    user: User[];

    @HasMany(() => Role)
    role: Role[];

    @HasMany(() => OrganizationCode)
    organizationCode: OrganizationCode[];
}

@Table
export class OrganizationCode extends Model {

    @PrimaryKey
    @Column({ primaryKey: true, type: DataType.UUID, defaultValue: DataType.UUIDV4 })
    id: string

    @Index
    @ForeignKey(() => Organization)
    @Column({ type: DataType.UUID, allowNull: true })
    organizationId: string;

    @Column({ defaultValue: false })
    asOrganization: boolean

    @Column({ allowNull: false })
    code: string
}

