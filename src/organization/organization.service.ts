import { Injectable } from '@nestjs/common';
import { CreateOrganizationCodeDto, CreateOrganizationDto } from './dto/create-organization.dto';
import { UpdateOrganizationDto } from './dto/update-organization.dto';
import { UserService } from 'src/user/user.service';
import { CommonService } from 'src/common/common.service';
import { ConfigService } from '@nestjs/config';
import { Sequelize } from 'sequelize-typescript';
import { Organization, OrganizationCode } from './entities/organization.entity';
import { InjectModel } from '@nestjs/sequelize';
import { WhereOptions } from 'sequelize';
import { Role } from 'src/role/entity/role.entity';
import { Permission, RolePermission } from 'src/permission/entity/permission.entity';
import { randomUUID } from 'crypto';
import { PaginationDTO } from 'src/configuration/consts';

@Injectable()
export class OrganizationService {
  constructor(
    private sequelize: Sequelize,
    @InjectModel(Organization)
    private organizationModel: typeof Organization,
    @InjectModel(OrganizationCode)
    private organizationCodeModel: typeof OrganizationCode,
    @InjectModel(Role)
    private roleModel: typeof Role,
    @InjectModel(Permission)
    private permissionModel: typeof Permission,
    @InjectModel(RolePermission)
    private rolePermissionModel: typeof RolePermission,
    private commonService: CommonService,
    private configService: ConfigService
  ) { }

  async create(createOrganizationDto: CreateOrganizationDto) {
    try {
      return await this.sequelize.transaction(async t => {
        const transactionHost = { transaction: t };

        let result = await this.organizationModel.create(
          { ...createOrganizationDto },
          transactionHost,
        );

        let result2 = await this.roleModel.create(
          { name: 'Admin', alias: 'admin', description: '', isActive: true, isAllowed: true, organizationId: result.dataValues.id },
          transactionHost,
        );

        let result2_1 = await this.roleModel.create(
          { name: 'General Employee', alias: 'general-employee', description: '', isActive: true, isAllowed: true, organizationId: result.dataValues.id },
          transactionHost,
        );

        let result3 = await this.permissionModel.create(
          { name: 'Admin', alias: 'admin', description: '', isActive: true, isAllowed: true, organizationId: result.dataValues.id },
          transactionHost,
        );

        let result3_1 = await this.permissionModel.create(
          { name: 'General Employee', alias: 'general-employee', description: '', isActive: true, isAllowed: true, organizationId: result.dataValues.id },
          transactionHost,
        );

        let result4 = await this.rolePermissionModel.create(
          { roleId: result2.dataValues.id, permissionId: result3.dataValues.id },
          transactionHost,
        );

        let result4_1 = await this.rolePermissionModel.create(
          { roleId: result2_1.dataValues.id, permissionId: result3_1.dataValues.id },
          transactionHost,
        );

        return this.commonService.successResponse({ organization: result.dataValues, role: result2.dataValues }, "Successful!");
      })
    } catch (error) {
      return this.commonService.errorResponse("Failed!", error);
    }

  }

  async findAll(condition: WhereOptions<any>) {
    try {
      const org = await this.organizationModel.findAll({
        where: condition,
      });
      return this.commonService.successResponse(org, "Successful!");
    } catch (error) {
      return this.commonService.errorResponse("Failed!", error);
    };

  }
  

  async findAndCOuntAll(condition: WhereOptions<any>, paginationDTO?: PaginationDTO) {
    try {
      const org = await this.organizationModel.findAndCountAll({
        where: condition, limit: paginationDTO.limit, offset: paginationDTO.offset
      });
      return this.commonService.successResponse(org, "Successful!");
    } catch (error) {
      return this.commonService.errorResponse("Failed!", error);
    };

  }

  async findOne(condition: WhereOptions<any>) {
    try {
      const org = await this.organizationModel.findOne({
        where: condition,
      });
      return this.commonService.successResponse(org, "Successful!");
    } catch (error) {
      return this.commonService.errorResponse("Failed!", error);
    };

  }

  async update(id: string, updateOrganizationDto: UpdateOrganizationDto) {
    try {
      const org = await this.organizationModel.update(updateOrganizationDto, {
        where: { id },
      });
      return this.commonService.successResponse(org, "Successful!");
    } catch (error) {
      return this.commonService.errorResponse("Failed!", error);
    };
  }

  remove(id: number) {
    return `This action removes a #${id} organization`;
  }

  async createOrganizationCode(auth: any, createOrganizationCodeDto: CreateOrganizationCodeDto) {
    try {
      const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName)
      const asOrganization = (createOrganizationCodeDto.asOrganization && superAdmin) ?? false

      let payload = { asOrganization, code: randomUUID(), }
      if (!asOrganization) {
        payload['organizationId'] = auth.userInfo.organizationId
      }
      const orgCode = await this.organizationCodeModel.create(payload)

      return this.commonService.successResponse(orgCode, "Successful!");
    } catch (error) {
      return this.commonService.errorResponse("Failed!", error);
    }
  }

  async getOrganizationCodes(auth: any, paginationDTO: PaginationDTO) {
    try {
      const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName)
      let condition = {}
      // if (!superAdmin) 
      condition["organizationId"] = auth.userInfo.organizationId;
      const orgCode = await this.organizationCodeModel.findAndCountAll({
        where: condition, limit: paginationDTO.limit, offset: paginationDTO.offset
      })
      console.log("orgCode->", orgCode)
      return this.commonService.successResponse(orgCode, "Successful!");
    } catch (error) {
      console.log(error)
      return this.commonService.errorResponse("Failed!", error);
    }
  }

  async getOrganizationCodeByCode(code: string): Promise<OrganizationCode> {
    try {
      const orgCode = await this.organizationCodeModel.findOne({
        where: {
          code: code
        }
      })
      return orgCode.dataValues
    } catch (error) {
      return null
    }
  }
}
