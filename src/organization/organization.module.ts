import { Module, forwardRef } from '@nestjs/common';
import { OrganizationService } from './organization.service';
import { OrganizationController } from './organization.controller';
import { CommonModule } from 'src/common/common.module';
import { JwtModule } from '@nestjs/jwt';
import { UserModule } from 'src/user/user.module';
import { SequelizeModule } from '@nestjs/sequelize';
import { Organization, OrganizationCode } from './entities/organization.entity';
import { Role } from 'src/role/entity/role.entity';
import { Permission, RolePermission } from 'src/permission/entity/permission.entity';

@Module({
  imports: [SequelizeModule.forFeature([Organization, Role, Permission, RolePermission, OrganizationCode]), forwardRef(() => UserModule), forwardRef(() => CommonModule), JwtModule],
  controllers: [OrganizationController],
  providers: [OrganizationService],
  exports: [OrganizationService]
})
export class OrganizationModule { }
