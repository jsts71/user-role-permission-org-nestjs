import { Organization } from "../entities/organization.entity"

export class CreateOrganizationDto extends Organization {
}

export class CreateOrganizationCodeDto {
    asOrganization?: boolean
}
