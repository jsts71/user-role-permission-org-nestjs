import { BeforeApplicationShutdown, Injectable, OnApplicationBootstrap, OnApplicationShutdown, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { UserService } from './user/user.service';
import { RoleService } from './role/role.service';
import { OrganizationService } from './organization/organization.service';
import { PermissionService } from './permission/permission.service';
import { CreateAuthDto } from './auth/dto/create-auth.dto';
import { CommonService } from './common/common.service';
import { AuthService } from './auth/auth.service';

@Injectable()
export class AppService implements OnModuleInit, OnApplicationBootstrap, OnModuleDestroy, BeforeApplicationShutdown, OnApplicationShutdown {

  constructor(
    private commonService: CommonService,
    private authService: AuthService,
    private userService: UserService,
    private roleService: RoleService,
    private organizationService: OrganizationService,
    private permissionService: PermissionService
  ) { }

  async onModuleInit() {
    console.log(`The module has been initialized. abir signatured!`);
  }

  async onApplicationBootstrap() {
    try {
      const checkInit = await this.commonService.checkInit()
      if (!checkInit) {
        await this.commonService.initializeApplication();
      }
      console.log(`The module has been started. abir signatured!`);
    } catch (error) {
      throw error
    }
  }

  async onModuleDestroy() {
    console.log("The module is destroying. abir signatured!");
  }

  beforeApplicationShutdown(signal: string) {
    console.log("The application is shutting down. abir signatured!", signal);
  }

  onApplicationShutdown(signal: string) {
    console.log("The application has shut down. abir signatured!", signal);
  }

  getHello(): string {
    return 'Hello World!';
  }
}
