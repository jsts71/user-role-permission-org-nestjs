import { Injectable, UnauthorizedException } from '@nestjs/common';
import { CheckContactOTPInput, UpdateAuthDto, UpdateUserPasswordByOTPInput } from './dto/update-auth.dto';
import { UserService } from 'src/user/user.service';
import { CommonService } from 'src/common/common.service';
import { CheckUserExistsDTO, CreateAuthDto, SignInDTO } from './dto/create-auth.dto';
import { ReAuthDto, SignInInterface } from './interface/auth.interface';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { EmailSignUpUserDto, PhoneSignUpUserDto, EmailOTPSignUpDTO, PhoneOTPSignUpDTO } from 'src/user/dto/create-user.dto';
import { OrganizationService } from 'src/organization/organization.service';
import { RoleService } from 'src/role/role.service';
import { Op } from 'sequelize';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private commonService: CommonService,
    private organizationService: OrganizationService,
    private roleService: RoleService,
    private readonly jwtService: JwtService,
    private configService: ConfigService
  ) { }


  async checkUserExists(checkUserExistsDTO: CheckUserExistsDTO) {
    try {
      const { phone, email, userName } = checkUserExistsDTO
      if (!(phone || email || userName)) return this.commonService.errorResponse('Nothing to check user existence!');

      let userPhoneExists
      let userEmailExists
      let userNameExists

      if (phone) userPhoneExists = await this.userService.findOne({ phone });
      if (email) userEmailExists = await this.userService.findOne({ email });
      if (userName) userEmailExists = await this.userService.findOne({ userName });

      if (userPhoneExists || userEmailExists || userNameExists) return this.commonService.successResponse({ userExists: true }, 'User already exists!');
      return this.commonService.successResponse({ userExists: false }, 'Users does not exists!');
    } catch (error) {
      return this.commonService.errorResponse("Fails to check!", error);
    }
  }

  async sign(contact: string, OTP: string) {
    try {
      const userOTPExists = await this.userService.findOneUserOTP({ contact });
      if (userOTPExists) await this.userService.updateOTP({ contact, OTP });
      else await this.userService.saveOTP({ contact, OTP });

      const isPhone = this.commonService.isPhone(contact)
      console.log("isPhone-->", isPhone)
      if (isPhone) {
        const sendPhoneOTP = await this.commonService.sendPhoneOTP(OTP, contact);
        if (sendPhoneOTP) return this.commonService.successResponse("Success", 'User OTP send to phone successfully!');
      } else {
        const sendEmailOTP = await this.commonService.sendEmailOTP(OTP, contact);
        if (sendEmailOTP) return this.commonService.successResponse("Success", 'User OTP send to email successfully!');
      }
      return this.commonService.errorResponse('User OTP failed to send. Try resend!');
    } catch (error) {
      return this.commonService.errorResponse("Register failed!", error);
    }
  }

  async signWithEmail(emailSignUpUserDto: EmailSignUpUserDto) {
    return await this.sign(emailSignUpUserDto.email, await this.commonService.createOTP())
  }

  async signWithPhone(phoneSignUpUser: PhoneSignUpUserDto) {
    return await this.sign(phoneSignUpUser.phone, await this.commonService.createOTP())
  }

  async signWithContact(contact: string) {
    return await this.sign(contact, await this.commonService.createOTP())
  }

  async verifyContactOTP(contact: string, OTP: string) {
    try {
      const userOTPExists = await this.userService.findOneUserOTP({ contact });
      if (!userOTPExists) return { state: false, message: 'You have no OTP request till now! Please follow steps' }
      await this.userService.removeOTP(userOTPExists.dataValues.id);
      await this.userService.removeExpiredOTP();

      const matchedOTP = userOTPExists.dataValues.OTP == OTP;
      if (!matchedOTP) return { state: false, message: 'OTP does not matched!' }

      const isDateExpired = await this.commonService.isDateExpired(userOTPExists.dataValues.updatedAt);
      if (isDateExpired) return { state: false, message: 'OTP time expired' }

      return { state: true, message: 'success' };
    } catch (error) {
      return { state: false, message: 'error' }
    }
  }

  async checkContactOTP(body: CheckContactOTPInput): Promise<any> {
    try {
      let { contact, OTP } = body
      const userOTPExists = await this.userService.findOneUserOTP({ contact });
      if (!userOTPExists) return this.commonService.errorResponse('You have no OTP request till now! Please follow steps');

      const matchedOTP = userOTPExists.dataValues.OTP == OTP;
      if (!matchedOTP) return this.commonService.errorResponse('OTP does not matched!');

      const isDateExpired = await this.commonService.isDateExpired(userOTPExists.dataValues.updatedAt);
      if (isDateExpired) return this.commonService.errorResponse('OTP time expired');

      return this.commonService.successResponse({ contact, OTP }, 'success');
    } catch (error) {
      return { state: false, message: 'error' }
    }
  }

  async emailOTPSignUp(verifyOTP: EmailOTPSignUpDTO) {
    try {
      let { state, message } = await this.verifyContactOTP(verifyOTP.email, verifyOTP.OTP);
      if (!state) return this.commonService.errorResponse(message);

      let userEmailExists = (await this.userService.findOne({ email: verifyOTP.email }));
      if (userEmailExists) userEmailExists = userEmailExists.dataValues
      else userEmailExists = (await this.userService.create({ email: verifyOTP.email, emailVerified: true, isAllowed: true } as any)).dataValues;
      delete userEmailExists.password;

      return this.commonService.successResponse(await this.commonService.createToken({ ...userEmailExists }), 'OTP verified');
    } catch (error) {
      return this.commonService.errorResponse("OTP failed to verify. Retry", error);
    }
  }

  async phoneOTPSignUp(verifyOTP: PhoneOTPSignUpDTO) {
    try {
      let { state, message } = await this.verifyContactOTP(verifyOTP.phone, verifyOTP.OTP);
      if (!state) return this.commonService.errorResponse(message);

      let userPhoneExists = (await this.userService.findOne({ phone: verifyOTP.phone }));
      if (userPhoneExists) userPhoneExists = userPhoneExists.dataValues
      else userPhoneExists = (await this.userService.create({ phone: verifyOTP.phone, phoneVerified: true, isAllowed: true } as any)).dataValues;
      delete userPhoneExists.password;

      return this.commonService.successResponse(await this.commonService.createToken({ ...userPhoneExists }), 'OTP verified');
    } catch (error) {
      return this.commonService.errorResponse("OTP failed to verify", error);
    }
  }

  async signUp(createAuthDto: CreateAuthDto) {

    try {
      let passwordMisMatch = createAuthDto.password !== createAuthDto.confirmPassword;
      if (passwordMisMatch) return this.commonService.errorResponse("Password doesn't match!", { Password: createAuthDto.password, confirmPassword: createAuthDto.confirmPassword });

      let userNameExists = await this.userService.findOne({ userName: createAuthDto.userName });
      if (userNameExists) return this.commonService.errorResponse("User with userName already exists!", { userName: createAuthDto.userName});
      let userEmailExists = await this.userService.findOne({ email: createAuthDto.email });
      if (userEmailExists) return this.commonService.errorResponse("User with email already exists!", { email: createAuthDto.email});
      let userPhoneExists = await this.userService.findOne({ phone: createAuthDto.phone });
      if (userPhoneExists) return this.commonService.errorResponse("User with phone already exists!", { phone: createAuthDto.phone});

      let phoneForOTP = createAuthDto.phoneForOTP == true
      
      if (!phoneForOTP) {
        let { state, message } = await this.verifyContactOTP(createAuthDto.email, createAuthDto.OTP)
        if (state) {
          createAuthDto.emailVerified = true
        }
        else return this.commonService.errorResponse(message);
      } else {
        let { state, message } = await this.verifyContactOTP(createAuthDto.phone, createAuthDto.OTP)
        if (state) {
          createAuthDto.phoneVerified = true
        }
        else return this.commonService.errorResponse(message);
      }
      createAuthDto['password'] = await this.commonService.bcryptPassword(createAuthDto.password);

      if (!createAuthDto.organizationCode) return this.commonService.errorResponse("Please provide org code")
      const orgCode = await this.organizationService.getOrganizationCodeByCode(createAuthDto.organizationCode)
      if (!orgCode) return this.commonService.errorResponse("Invalid org code")

      createAuthDto.isAllowed = false
      if (orgCode.asOrganization) createAuthDto.userAsOrg = true
      else createAuthDto.organizationId = orgCode.organizationId
      
      const serviceResponse = await this.userService.create(createAuthDto);
      return this.commonService.successResponse(serviceResponse, 'User registered successfully!');
    } catch (error) {
      console.log(error)
      return this.commonService.errorResponse("Register failed!", error);
    }
  }

  async checkOrgCode(code: string) {
    try {
      if (!code) return this.commonService.errorResponse("Please provide org code")
      const orgCode = await this.organizationService.getOrganizationCodeByCode(code)
      if (!orgCode) return this.commonService.errorResponse("Invalid org code")

      if (orgCode.asOrganization) return this.commonService.successResponse({code}, 'Validated User as org code!');
      else return this.commonService.successResponse({code}, 'Validated User as org employee code!');
      
    } catch (error) {
      console.log(error)
      return this.commonService.errorResponse("Something went wrong!", error);
    }
  }

  findAll() {
    try {
      return this.commonService.errorResponse(`This action returns all auth`);
    } catch (error) {
      return this.commonService.errorResponse("`This action returns all auth` failed!", error);
    }
  }


  findOne(id: number) {
    return `This action returns a #${id} auth`;
  }

  update(id: number, updateAuthDto: UpdateAuthDto) {
    return `This action updates a #${id} auth`;
  }

  remove(id: number) {
    return `This action removes a #${id} auth`;
  }

  // Record<string, any>
  async signIn(signInDto: SignInDTO): Promise<any> {
    try {
      let user;
      if (signInDto.userName) user = await this.userService.findOne({ userName: signInDto.userName, isActive: true, isAllowed: true });
      else if (signInDto.email) user = await this.userService.findOne({ email: signInDto.email, emailVerified: true, isActive: true, isAllowed: true });
      else if (signInDto.phone) user = await this.userService.findOne({ phone: signInDto.phone, phoneVerified: true, isActive: true, isAllowed: true });
      else return this.commonService.errorResponse("User doesn't exists!", signInDto);

      const match = await this.commonService.isPasswordMatch(signInDto.password, user.password);
      if (!match) {
        return this.commonService.errorResponse("Unauthorized!", new UnauthorizedException());
      }

      let payload = { ...user.dataValues, rememberMe: signInDto.rememberMe }
      delete payload.password

      let response = await this.commonService.createToken(payload)
      return this.commonService.successResponse(response, 'User login successfully!');
    } catch (error) {
      return this.commonService.errorResponse("Login failed!", error);
    }
  }

  async updatePasswordByOTP(updateUserPasswordByOTPInput: UpdateUserPasswordByOTPInput) {
    try {

      let { state, message } = await this.verifyContactOTP(updateUserPasswordByOTPInput.contact, updateUserPasswordByOTPInput.OTP)
      if (!state) return this.commonService.errorResponse("Invalid OTP.");

      const user = await this.userService.findOne({
        [Op.or]: [
          { email: updateUserPasswordByOTPInput.contact },
          { phone: updateUserPasswordByOTPInput.contact }
        ]
      })

      if (!user) {
        const isPhone = this.commonService.isPhone(updateUserPasswordByOTPInput.contact);
        if (isPhone) await this.userService.create({ password: await this.commonService.bcryptPassword(updateUserPasswordByOTPInput.password), phone: updateUserPasswordByOTPInput.contact, phoneVerified: true, isAllowed: true, isActive: true } as any)
        else await this.userService.create({ password: await this.commonService.bcryptPassword(updateUserPasswordByOTPInput.password), email: updateUserPasswordByOTPInput.contact, emailVerified: true, isAllowed: true, isActive: true } as any)
        return this.commonService.errorResponse("No user found, We create a user account for you with your provided constrains.");
      }

      await this.userService.update({ id: user.id, body: { password: await this.commonService.bcryptPassword(updateUserPasswordByOTPInput.password) } as any })
      return this.commonService.successResponse("response", "User password updated successfully.");
    } catch (error) {
      console.log(error)
      return this.commonService.errorResponse("User password update failed!", error);
    }
  }


  // Record<string, any>
  async reAuth(reAuthDto: ReAuthDto, token: any): Promise<any> {
    try {
      const checkTokenForReAuth = await this.commonService.checkTokenForReAuth(token.split(" ")[1])
      if (!checkTokenForReAuth) return this.commonService.errorResponse("Token dead");
      const reAuth = await this.commonService.createToken(checkTokenForReAuth)
      return this.commonService.successResponse({ token: reAuth }, 'User login successfully!');
    } catch (error) {
      return this.commonService.errorResponse("Login failed!", error);
    }
  }
}
