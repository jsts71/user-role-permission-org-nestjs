import { PartialType } from '@nestjs/mapped-types';
import { CreateAuthDto } from './create-auth.dto';

export class UpdateAuthDto extends PartialType(CreateAuthDto) {}


export class UpdateUserPasswordByOTPInput {

    password: string;

    OTP: string;

    contact: string;

}

export class CheckContactOTPInput {

    OTP: string;
    contact: string;

}