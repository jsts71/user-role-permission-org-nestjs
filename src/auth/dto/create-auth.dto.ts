import { IsBoolean, IsEmail, IsEmpty, IsNotEmpty, IsOptional, IsPhoneNumber, IsString } from "class-validator";
import { User } from "src/user/entity/user.entity";

export class CreateAuthDto extends User {
    @IsNotEmpty()
    confirmPassword: string;

    @IsNotEmpty()
    OTP: string;

    @IsNotEmpty()
    phoneForOTP: boolean = false;

    organizationCode?: string;

}

export class CheckUserExistsDTO {

    @IsEmail()
    @IsOptional()
    email?: string;

    @IsPhoneNumber()
    @IsOptional()
    phone?: string;

    @IsString()
    @IsOptional()
    userName?: string;
}
export class SignInDTO {

    @IsOptional()
    @IsString()
    userName?: string;

    @IsOptional()
    @IsEmail()
    email?: string;

    @IsOptional()
    @IsPhoneNumber()
    phone?: string;

    @IsString()
    @IsNotEmpty()
    password: string;

    @IsBoolean()
    @IsOptional()
    rememberMe?: boolean = false
} 