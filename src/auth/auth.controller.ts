import { Controller, Get, Post, Body, Patch, Param, Delete, HttpCode, HttpStatus, Req, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CheckUserExistsDTO, CreateAuthDto } from './dto/create-auth.dto';
import { CheckContactOTPInput, UpdateAuthDto, UpdateUserPasswordByOTPInput } from './dto/update-auth.dto';
import { Request, Response } from 'express';
import { ReAuthDto, SignInInterface } from './interface/auth.interface';
import { EmailSignUpUserDto, PhoneSignUpUserDto, EmailOTPSignUpDTO, PhoneOTPSignUpDTO } from 'src/user/dto/create-user.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) { }

  @HttpCode(HttpStatus.OK)
  @Post('check-user-exists')
  async checkUserExists(@Body() checkUserExistsDTO: CheckUserExistsDTO, @Req() request: Request, @Res() response: Response) {
    const serviceResponse = await this.authService.checkUserExists(checkUserExistsDTO);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @Post('sign-with-email')
  async signWithMail(@Body() body: EmailSignUpUserDto, @Req() request: Request, @Res() response: Response) {
    const serviceResponse = await this.authService.signWithEmail(body);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @Post('sign-with-phone')
  async signWithPhone(@Body() body: PhoneSignUpUserDto, @Req() request: Request, @Res() response: Response) {
    const serviceResponse = await this.authService.signWithPhone(body);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @Post('sign-with-contact')
  async signWithContact(@Body() body: { contact: string }, @Req() request: Request, @Res() response: Response) {
    const serviceResponse = await this.authService.signWithContact(body.contact);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @Post('email-otp-signup')
  async verifyEmailOTP(@Body() body: EmailOTPSignUpDTO, @Req() request: Request, @Res() response: Response) {
    const serviceResponse = await this.authService.emailOTPSignUp(body);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @Post('phone-otp-signup')
  async verifyPhoneOTP(@Body() body: PhoneOTPSignUpDTO, @Req() request: Request, @Res() response: Response) {
    const serviceResponse = await this.authService.phoneOTPSignUp(body);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @HttpCode(HttpStatus.OK)
  @Post('signup')
  async signUp(@Body() createAuthDto: CreateAuthDto, @Req() request: Request, @Res() response: Response) {
    const serviceResponse = await this.authService.signUp(createAuthDto);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @HttpCode(HttpStatus.OK)
  @Post('check/org-code')
  async checkOrgCode(@Body() body: {code:string}, @Req() request: Request, @Res() response: Response) {
    const serviceResponse = await this.authService.checkOrgCode(body.code);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @Get()
  findAll() {
    return this.authService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.authService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAuthDto: UpdateAuthDto) {
    return this.authService.update(+id, updateAuthDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.authService.remove(+id);
  }

  @HttpCode(HttpStatus.OK)
  @Post('login')
  async signIn(@Body() signInDto: SignInInterface, @Req() request: Request, @Res() response: Response) {
    const serviceResponse = await this.authService.signIn(signInDto);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @HttpCode(HttpStatus.OK)
  @Post('check/otp')
  async checkContactOTP(@Body() body: CheckContactOTPInput, @Req() request: Request, @Res() response: Response) {
    const serviceResponse = await this.authService.checkContactOTP(body);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @HttpCode(HttpStatus.OK)
  @Post('update-password/by-otp')
  async updatePasswordByOTP(@Body() updateUserPasswordByOTPInput: UpdateUserPasswordByOTPInput, @Req() request: Request, @Res() response: Response) {
    const serviceResponse = await this.authService.updatePasswordByOTP(updateUserPasswordByOTPInput);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @HttpCode(HttpStatus.OK)
  @Post('re-auth')
  async reAuth(@Body() reAuthDto: ReAuthDto, @Req() request: Request, @Res() response: Response) {
    const token = request.headers.authorization as any;
    const serviceResponse = await this.authService.reAuth(reAuthDto, token);
    return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
  }

  @HttpCode(HttpStatus.OK)
  @Post('check/token')
  async checkToken(@Req() request: Request, @Res() response: Response) {
    return response.send({ message: "Valid token" });
  }
}
