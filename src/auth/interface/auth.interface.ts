export interface SignInInterface {
    userName?: string;
    email?: string;
    phone?: string;
    password: string;
}
export interface ReAuthDto {
    refreshToken: string;
} 