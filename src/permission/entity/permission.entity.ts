import { BelongsTo, BelongsToMany, Column, DataType, ForeignKey, Index, Model, PrimaryKey, Table, Unique } from 'sequelize-typescript';
import { Organization } from 'src/organization/entities/organization.entity';
import { Role } from 'src/role/entity/role.entity';

@Table
export class Permission extends Model {
    

    @PrimaryKey
    @Column({ primaryKey: true, type: DataType.UUID, defaultValue: DataType.UUIDV4 })
    id: string

    @Column({allowNull: false})
    name: string;

    @Column({allowNull: false})
    alias: string;

    @Column({allowNull: false})
    description: string;

    @Column({ defaultValue: true })
    isActive: boolean;
    
    @BelongsToMany(()=> Role, ()=> RolePermission)
    role: Role;

    @Index
    @ForeignKey(() => Organization)
    @Column({allowNull: false, type: DataType.UUID})
    organizationId: string;

    @BelongsTo(() => Organization)
    organization: Organization;
}

@Table
export class RolePermission extends Model {

    @PrimaryKey
    @Column({ primaryKey: true, type: DataType.UUID, defaultValue: DataType.UUIDV4 })
    id: string

    @Index
    @ForeignKey(()=> Role)
    @Column({allowNull: false, type: DataType.UUID})
    roleId: string;

    @Index
    @ForeignKey(()=> Permission)
    @Column({allowNull: false, type: DataType.UUID})
    permissionId: string
}