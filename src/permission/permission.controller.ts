import { Body, Controller, Get, HttpStatus, Param, Post, Put, Req, Res } from '@nestjs/common';
import { PermissionService } from './permission.service';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { Request, Response } from 'express';
import { CommonService } from 'src/common/common.service';
import { RoleService } from 'src/role/role.service';

@Controller('permission')
export class PermissionController {
    constructor(
        private roleService: RoleService,
        private permissionService: PermissionService,
        private commonService: CommonService,
    ) { }

    @Post()
    async create(@Body() createPermissionDto: CreatePermissionDto, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const orgAdmin = await this.commonService.checkOrganizationAdmin(auth);
        if (!orgAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

        const alias = this.commonService.toNoSpaceLowerCase(createPermissionDto.name);
        const organizationId = auth.userInfo.organizationId
        const isExists = await this.permissionService.findOne({ alias, organizationId })
        if (isExists.success) return response.status(HttpStatus.BAD_REQUEST).send("Already exists")

        createPermissionDto.alias = alias;
        createPermissionDto.organizationId = organizationId
        const serviceResponse = await this.permissionService.create(createPermissionDto);
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Get()
    async findAll(@Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName)
        if (!superAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

        const serviceResponse = await this.permissionService.findAll({});
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Get(":id")
    async findOne(@Param('id') id: string, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName)
        const orgAdmin = await this.commonService.checkOrganizationAdmin(auth)
        if (!(superAdmin || orgAdmin)) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

        const serviceResponse = await this.permissionService.findOne({ id });
        if(!superAdmin && serviceResponse?.data?.organizationId !== auth.userInfo.organizationId) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized");

        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Get("user/organization")
    async findAllOfUser(@Req() request: Request, @Res() response: Response) {
        const auth = request.headers.data as any;
        const orgAdmin = await this.commonService.checkOrganizationAdmin(auth);
        if (!orgAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized");

        const serviceResponse = await this.permissionService.findAll({ organizationId: auth.userInfo.organizationId });
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Get("organization/:organizationId")
    async findAllofOrg(@Param('organizationId') organizationId: string, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName);
        if (!superAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized");

        const serviceResponse = await this.permissionService.findAllConditional({ organizationId });
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Get("role/:roleId")
    async findAllofRole(@Param('roleId') id: string, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName)
        const orgAdmin = await this.commonService.checkOrganizationAdmin(auth);
        if (!(superAdmin || orgAdmin)) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized");

        const role = await this.roleService.findOne({ id });
        if (!superAdmin && role?.data?.organizationId !== auth.userInfo.organizationId) return response.status(HttpStatus.BAD_REQUEST).send("Unauthorized role")
        if (!role.data) return response.status(HttpStatus.BAD_REQUEST).send("Role not found");

        const serviceResponse = await this.permissionService.findByRole(id);
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Put(":id")
    async update(@Param('id') id: string, @Body() updateDto: CreatePermissionDto, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const orgAdmin = await this.commonService.checkOrganizationAdmin(auth);
        if (!orgAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized");

        let alias
        if (updateDto.name) alias = this.commonService.toNoSpaceLowerCase(updateDto.name);
        if (updateDto.organizationId) delete updateDto.organizationId

        const organizationId = auth.userInfo.organizationId
        const isExists = await this.permissionService.findOne({ alias, organizationId })

        if (isExists.success) return response.status(HttpStatus.BAD_REQUEST).send("Already exists")
        if (alias == 'general-employee') return response.status(HttpStatus.BAD_REQUEST).send("You can't change this!")

        updateDto.alias = alias;
        const serviceResponse = await this.permissionService.update(id, updateDto);
        if(serviceResponse?.data?.organizationId !== auth.userInfo.organizationId) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized");

        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }
}
