import { Injectable } from '@nestjs/common';
import { Sequelize } from 'sequelize-typescript';
import { Permission, RolePermission } from './entity/permission.entity';
import { InjectModel } from '@nestjs/sequelize';
import { CommonService } from 'src/common/common.service';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { WhereOptions } from 'sequelize';

@Injectable()
export class PermissionService {

  constructor(
    private sequelize: Sequelize,
    @InjectModel(Permission)
    private permissionModel: typeof Permission,
    @InjectModel(RolePermission)
    private rolePermissionModel: typeof RolePermission,
    private commonService: CommonService,
  ) { }

  async create(createPermissionDto: CreatePermissionDto) {
    try {
      return await this.sequelize.transaction(async t => {
        const transactionHost = { transaction: t };
        let result = await this.permissionModel.create(
          { ...createPermissionDto },
          transactionHost,
        );
        return this.commonService.successResponse(result.dataValues, "Successful!");
      })
    } catch (error) {
      return this.commonService.errorResponse("Failed!", error);
    }
  }

  async findAll(condition: WhereOptions<any>) {
    try {
      const permission = await this.permissionModel.findAll({
        where: condition,
      });
      return this.commonService.successResponse(permission, "Successful!");
    } catch (error) {
      return this.commonService.errorResponse("Failed!", error);
    };

  }

  async findAllConditional(condition: WhereOptions<any>) {
    try {
      const permission = await this.permissionModel.findAll({
        where: condition,
      });
      return this.commonService.successResponse(permission, "Successful!");
    } catch (error) {
      return this.commonService.errorResponse("Failed!", error);
    };

  }

  async findOne(condition: WhereOptions<any>) {
    try {
      const permission = await this.permissionModel.findOne({
        where: condition,
      });
      return this.commonService.successResponse(permission, "Successful!");
    } catch (error) {
      return this.commonService.errorResponse("Failed!", error);
    };

  }

  async findByRole(roleId: string) {
    try {
      const rolePermissions = await this.rolePermissionModel.findAll({ where: { roleId } })
      const permissionIds = rolePermissions.map((rolePermission) => rolePermission.permissionId)
      const permission = await this.permissionModel.findAll({
        where: {
          id: permissionIds
        },
      });
      return this.commonService.successResponse(permission, "Successful!");
    } catch (error) {
      return this.commonService.errorResponse("Failed!", error);
    };

  }
  
  async update(id: string, updateDto: CreatePermissionDto) {
    try {
      const permission = await this.permissionModel.update(updateDto, {
        where: {
          id: id
        },
      });
      return this.commonService.successResponse(permission, "Successful!");
    } catch (error) {
      return this.commonService.errorResponse("Failed!", error);
    };
  }

}
