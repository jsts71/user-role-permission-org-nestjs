export enum phases {
    "DEVELOPMENT",
    "PRODUCTION"
}
export interface DatabaseConfigurationInterface {
    type: any;
    host: string;
    port: number;
    username: string;
    password: string;
    database: string;
    synchronize: boolean;
}

export interface Settings {
    OTPLifeTime: number
    saltOrRounds: number
    jwtSecret: string
    privateKey: string
    accessTokenLife: number
    refreshTokenLife: number
    cors: string[]
    socket: string
}

export interface SuperAdmin {
    firstName: string;
    lastName: string;
    userName: string;
    email: string;
    phone: string;
    password: string;
    organizationName: string;
    role: string;
    permission: string;
    employeeRole: string;
    employeePermission: string;
}

export enum operation {
    Create = "CREATE",
    Read = "READ",
    Update = "UPDATE",
    Delete = "DELETE"
}

export enum permission {
    "PERMISSION",
    "ORGANIZATION",
    "ROLE",
    "USER",
    "AUTH"
}

export class PaginationDTO {

    limit: number = 2
    offset: number = 0
}