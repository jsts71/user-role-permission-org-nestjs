import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { SequelizeModuleOptions, SequelizeOptionsFactory } from "@nestjs/sequelize";
import { DatabaseConfigurationInterface } from "./consts";

@Injectable()
export class SequelizeConfigService implements SequelizeOptionsFactory {
  constructor(private configService: ConfigService) { }

  createSequelizeOptions(connectionName?: string): SequelizeModuleOptions | Promise<SequelizeModuleOptions> {

    const database = this.configService.get('database')
    let dbConfig: DatabaseConfigurationInterface;

    database === 'postgres' ? dbConfig = this.configService.get<DatabaseConfigurationInterface>('db.postgres') :
      database === 'mysql' ? dbConfig = this.configService.get<DatabaseConfigurationInterface>('db.mysql') : null;

    let service = {
      host: dbConfig.host,
      port: dbConfig.port,
      username: dbConfig.username,
      password: dbConfig.password,
      database: dbConfig.database,
      synchronize: dbConfig.synchronize,
      autoLoadModels: true,
    }

    database === 'postgres' ? service['dialect'] = dbConfig.type :
      database === 'mysql' ? service['type'] = dbConfig.type : null

    return service
  }
}
