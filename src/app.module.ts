import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { OrganizationModule } from './organization/organization.module';
import { PermissionModule } from './permission/permission.module';
import { RoleModule } from './role/role.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { validate } from './configuration/env.validation';
import genericConfiguration from './configuration/generic.configuration';
import { SequelizeModule } from '@nestjs/sequelize';
import { JwtModule } from '@nestjs/jwt';
import { SequelizeConfigService } from './configuration/configuration.service';
import { AuthModule } from './auth/auth.module';
import { CommonModule } from './common/common.module';
import { User, UserOTP } from './user/entity/user.entity';
import { Organization, OrganizationCode } from './organization/entities/organization.entity';
import { Role } from './role/entity/role.entity';
import { Permission, RolePermission } from './permission/entity/permission.entity';
import { LoggerMiddleware } from './middleware';

import { io } from "socket.io-client";
const socket = io("http://localhost:3000");
socket.on('onCurrentEvent', (v) => {
  console.log(v)
})


@Module({
  imports: [
    ConfigModule.forRoot({
      validate,
      load: [genericConfiguration],
      isGlobal: true,
      validationOptions: {
        allowUnknown: false,
        abortEarly: true,
      },
    }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('settings.jwtSecret'),
        global: true,
        signOptions: { expiresIn: '60s' },
      }),
      inject: [ConfigService],
    }),
    SequelizeModule.forFeature([User, Organization, Role, Permission, RolePermission, UserOTP, OrganizationCode]),
    SequelizeModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useClass: SequelizeConfigService
    }),
    UserModule, OrganizationModule, PermissionModule, RoleModule, AuthModule, CommonModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes('user', 'user', 'permission', 'organization', 'role', 'auth/check/token')

  }
  
}
