import { Module, forwardRef } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { User, UserOTP } from './entity/user.entity';
import { OrganizationModule } from 'src/organization/organization.module';
import { CommonModule } from 'src/common/common.module';
import { Role } from 'src/role/entity/role.entity';

@Module({
  imports: [SequelizeModule.forFeature([User, UserOTP, Role]), forwardRef (() =>  OrganizationModule), forwardRef (() =>  CommonModule)],
  providers: [UserService],
  controllers: [UserController],
  exports: [UserService],
})
export class UserModule {}
