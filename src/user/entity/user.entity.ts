import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, HasOne, Index, Model, PrimaryKey, Table, Unique } from 'sequelize-typescript';
import { Organization } from 'src/organization/entities/organization.entity';
import { Role } from 'src/role/entity/role.entity';

@Table
export class User extends Model {

    @PrimaryKey
    @Column({ primaryKey: true, type: DataType.UUID, defaultValue: DataType.UUIDV4 })
    id: string

    @Column
    firstName: string;

    @Column
    lastName: string;

    @Index
    @Unique
    @Column
    userName: string;

    @Index
    @Unique
    @Column
    email: string;

    @Unique
    @Column
    phone: string;

    @Column
    password: string;

    @Column({ defaultValue: true })
    isActive: boolean;

    @Column({ defaultValue: false })
    isAllowed: boolean = false;

    @Column({ defaultValue: false })
    emailVerified: boolean;

    @Column({ defaultValue: false })
    phoneVerified: boolean;

    @Column({ defaultValue: false })
    userAsOrg: boolean = false;

    @Index
    @ForeignKey(() => Organization)
    @Column({ type: DataType.UUID })
    organizationId: string;

    @BelongsTo(() => Organization)
    organization: Organization;

    @Index
    @ForeignKey(() => Role)
    @Column({ type: DataType.UUID })
    roleId: number;

    @BelongsTo(() => Role)
    role: Role;
}

@Table
export class UserOTP extends Model {

    @PrimaryKey
    @Column({ primaryKey: true, type: DataType.UUID, defaultValue: DataType.UUIDV4 })
    id: number

    @Column({ allowNull: false })
    contact: string;

    @Column
    OTP: string;
}