import { IsEmail, IsNotEmpty, IsPhoneNumber } from "class-validator";

export class CreateUserDto { }

export class SignUpUserDto {

    @IsNotEmpty()
    firstName: string;

    @IsNotEmpty()
    lastName: string;

    @IsNotEmpty()
    userName: string;

    @IsNotEmpty()
    email: string;

    @IsNotEmpty()
    phone: string;

    @IsNotEmpty()
    password: string;

}

export class PhoneSignUpUserDto {
    @IsPhoneNumber()
    @IsNotEmpty()
    phone: string;
}

export class EmailSignUpUserDto {
    @IsEmail()
    @IsNotEmpty()
    email: string;
}

export class EmailOTPSignUpDTO {
    @IsEmail()
    @IsNotEmpty()
    email: string;

    @IsNotEmpty()
    OTP: string;
}

export class PhoneOTPSignUpDTO {
    @IsPhoneNumber()
    @IsNotEmpty()
    phone: string;

    @IsNotEmpty()
    OTP: string;
}

export class CreateUserOTPDto {
    @IsNotEmpty()
    contact: string;

    @IsNotEmpty()
    OTP: string;
}
