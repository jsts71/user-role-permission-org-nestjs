import { IsEmail, IsNotEmpty, IsPhoneNumber } from 'class-validator';

export class UpdateUserDto {

    // id?: string;
    firstName?: string;
    lastName?: string;
    userName?: string;
    // email?: string;
    // password?: string;
    // phone?: string;
    // organizationId?: number;
    // isActive?: boolean;
    // isAllowed?: boolean;
    // updatedAt?: any;
    // createdAt?: any;
    // roleId?: number;
    // userAsOrg?: boolean;
}
export class UpdateUserPassword {
    oldPassword: string;
    @IsNotEmpty()
    password: string;
    confirmPassword: string;
}
export class UpdateUserEmail {
    @IsEmail()
    email: string;
    @IsNotEmpty()
    otp?: string;
}
export class UpdateUserPhone {
    @IsPhoneNumber()
    phone: string;
    @IsNotEmpty()
    otp?: string;
}

export class AllowUsersDTO {
    @IsNotEmpty()
    ids: string[]
}
