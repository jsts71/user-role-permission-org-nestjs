import { Inject, Injectable } from '@nestjs/common';
import { User, UserOTP } from './entity/user.entity';
import { InjectConnection, InjectModel } from '@nestjs/sequelize';
import { CreateUserDto, CreateUserOTPDto, PhoneSignUpUserDto } from './dto/create-user.dto';
import { Sequelize } from 'sequelize-typescript';
import { Op, UUIDV4, WhereOptions } from 'sequelize';
import { CreateAuthDto } from 'src/auth/dto/create-auth.dto';
import { OrganizationService } from 'src/organization/organization.service';
import { CommonService } from 'src/common/common.service';
import { Role } from 'src/role/entity/role.entity';
import { UpdateUserDto, UpdateUserEmail, UpdateUserPassword, UpdateUserPhone } from './dto/update-user.dto';
@Injectable()
export class UserService {

    constructor(
        private sequelize: Sequelize,
        private commonService: CommonService,
        @InjectModel(User)
        private userModel: typeof User,
        @InjectModel(Role)
        private roleModel: typeof Role,
        @InjectModel(UserOTP)
        private userOTPModel: typeof UserOTP,
        private organizationService: OrganizationService,
    ) { }

    async allowUserAsOrg(users: User[]) {
        try {
            for (let index in users) {
                let user = users[index].dataValues

                let data = {
                    name: "ORG " + user.userName,
                    alias: "org-" + this.commonService.toNoSpaceLowerCase(user.userName),
                    isAllowed: true,
                    description: " "
                }
                let result = await this.organizationService.create(data as any);

                let result2 = await this.userModel.update(
                    { isAllowed: true, organizationId: result.data.organization.id, roleId: result.data.role.id },
                    {
                        where: {
                            id: users[index].dataValues.id
                        }
                    }
                );
                let result3 = await this.update({ id: users[index].dataValues.id, body: { isAllowed: true, organizationId: result.data.organization.id, roleId: result.data.role.id } as any })
            }
            return this.commonService.successResponse(true, 'User allowed successfully!');
        } catch (error) {
            console.log(error)
            return this.commonService.errorResponse("Allowing user failed!", error);
        }
    }

    async allowUserInOrg(users: User[]) {

        try {
            for (let index in users) {
                let result = await this.roleModel.findOne({
                    where: {
                        organizationId: users[index].dataValues.organizationId,
                        alias: 'general-employee'
                    }
                })
                let result3 = await this.update({ id: users[index].dataValues.id, body: { isAllowed: true, roleId: result.dataValues.id } as any })
            }
            return this.commonService.successResponse(true, 'User allowed successfully!');
        } catch (error) {
            console.log(error)
            return this.commonService.errorResponse("Allowing user failed!", error);
        }
    }

    async create(createAuthDto: CreateAuthDto) {
        try {
            const userAsOrg = createAuthDto.userAsOrg;
            return await this.sequelize.transaction(async t => {
                const transactionHost = { transaction: t };
                let result2 = await this.userModel.create(
                    { ...createAuthDto },
                    transactionHost,
                );
                return result2;
            })
        } catch (error) {
            throw error;
        }
    }

    async update(updateDTO: { id: string, body: CreateAuthDto }) {
        try {
            let result = await this.userModel.update(
                { ...updateDTO.body },
                {
                    where: {
                        id: updateDTO.id
                    }
                }
            );
            return result;

        } catch (error) {
            throw error;
        }
    }

    async updateResponse(id: string, body: any) {
        try {
            if (body.password) delete body.password;
            if (body.email) delete body.email;
            if (body.phone) delete body.phone;
            if (body.phoneVerified != undefined) delete body.phoneVerified;
            if (body.emailVerified != undefined) delete body.emailVerified;
            const response = await this.update({ id, body });
            return this.commonService.successResponse(response, 'User updated successfully!');
        } catch (error) {
            return this.commonService.errorResponse("Updating user failed!", error);
        }
    }

    async updateUserPasswordResponse(id: string, body: any) {
        try {
            const user = await this.findOne({ id })

            if (user.password) {
                if (body.password !== body.confirmPassword) return this.commonService.errorResponse("Confirm password doesn't match!");
                if (body.password === body.oldPassword) return this.commonService.errorResponse("Password remain unchanged!");
                const match = await this.commonService.isPasswordMatch(body.oldPassword, user.password);
                if (!match) return this.commonService.errorResponse("Password doesn't match!");
            }
            if (body.password) body.password = await this.commonService.bcryptPassword(body.password)
            const response = await this.update({ id, body })
            return this.commonService.successResponse(response, 'User updated successfully!');
        } catch (error) {
            return this.commonService.errorResponse("Updating user failed!", error);
        }
    }

    async updateUserEmailResponse(id: string, body: UpdateUserEmail) {
        try {
            // Check email exists in any user
            // Fetch OTP by contact if not exists send OTP request not found
            // Check OTP matched? If not return OTP not matched
            // Check OTP time expired? if yes return OTP time expired
            // All ok, then Change email

            const emailExists = await this.findOne({ email: body.email });
            if (emailExists) return this.commonService.errorResponse("Email already exists!");
            const response = await this.findOneUserOTP({ contact: body.email });
            if (!response) return this.commonService.errorResponse("OTP request not found!");
            if (response.OTP !== body.otp) return this.commonService.errorResponse("OTP doesn't match!");
            const isExpired = await this.commonService.isDateExpired(response.updatedAt);
            if (isExpired) return this.commonService.errorResponse("OTP time expired! Resend OTP.", { isExpired });
            await this.removeOTP(response.id);
            let updateResponse = await this.update({ id, body: { email: body.email, emailVerified: true } as any });
            return this.commonService.successResponse(updateResponse, 'User updated successfully!');
        } catch (error) {
            return this.commonService.errorResponse("Updating user failed!", error);
        }
    }

    async updateUserPhoneResponse(id: string, body: UpdateUserPhone) {
        try {
            // Check phone exists in any user
            // Fetch OTP by contact if not exists send OTP request not found
            // Check OTP matched? If not return OTP not matched
            // Check OTP time expired? if yes return OTP time expired
            // All ok, then Change phone

            const phoneExists = await this.findOne({ phone: body.phone });
            if (phoneExists) return this.commonService.errorResponse("Phone already exists!");
            const response = await this.findOneUserOTP({ contact: body.phone });
            if (!response) return this.commonService.errorResponse("OTP request not found!");
            if (response.OTP !== body.otp) return this.commonService.errorResponse("OTP doesn't match!");
            const isExpired = await this.commonService.isDateExpired(response.updatedAt);
            if (isExpired) return this.commonService.errorResponse("OTP time expired! Resend OTP.", { isExpired });
            await this.removeOTP(response.id)
            let updateResponse = await this.update({ id, body: { phone: body.phone, phoneVerified: true } as any })
            return this.commonService.successResponse(updateResponse, 'User updated successfully!');
        } catch (error) {
            return this.commonService.errorResponse("Updating user failed!", error);
        }
    }

    async saveOTP(createUserOTP: CreateUserOTPDto) {
        try {
            return await this.sequelize.transaction(async t => {
                const transactionHost = { transaction: t };
                let result = await this.userOTPModel.create(
                    { ...createUserOTP },
                    transactionHost,
                );
                return result;
            })
        } catch (error) {
            throw error;
        }
    }

    async updateOTP(createUserOTP: CreateUserOTPDto) {
        try {
            let result = await this.userOTPModel.update(
                { OTP: createUserOTP.OTP },
                {
                    where: {
                        contact: createUserOTP.contact
                    }
                },
            );
            return result;
        } catch (error) {
            throw error;
        }
    }

    async removeOTP(id: number) {
        try {
            let result = await this.userOTPModel.destroy(
                {
                    where: {
                        id
                    }
                },
            );
            return result;
        } catch (error) {
            throw error;
        }
    }

    async removeExpiredOTP() {
        try {
            const dateTime = await this.commonService.toPostGresTimeStamp(new Date(await this.commonService.changeDateTime(new Date(), '-')))
            let result1 = await this.userOTPModel.findAll({
                where: {
                    updatedAt: {
                        [Op.lt]: dateTime
                    }
                }
            });
            let ids = result1.map((phoneOTP) => phoneOTP.id)

            await this.userOTPModel.destroy(
                {
                    where: {
                        id: ids
                    }
                },
            );
            return ids;
        } catch (error) {
            throw error;
        }
    }

    async findOne(condition: WhereOptions<any>): Promise<User | undefined> {
        try {
            const user = await this.userModel.findOne({
                where: condition,
            });
            return user;
        } catch (error) {
            throw error;
        };
    }

    async fetch(id: string) {
        try {
            const response = await this.findOne({ id })
            delete response.dataValues.password;
            return this.commonService.successResponse(response, 'User details fetched successfully!');
        } catch (error) {
            return this.commonService.errorResponse("Fetching user details failed!", error);
        }
    }

    async findAll(condition: WhereOptions<any>): Promise<User[] | undefined> {
        try {
            const user = await this.userModel.findAll({
                where: condition,
            });
            return user;
        } catch (error) {
            throw error;
        };
    }

    async findAllResponse(condition: WhereOptions<any>) {
        try {
            let users = await this.userModel.findAll({
                attributes: { exclude: ['password'] },
                where: condition,
            });
            return this.commonService.successResponse(users, 'User details fetched successfully!');
        } catch (error) {
            return this.commonService.errorResponse("Fetching user details failed!", error);
        }
    }

    async findOneUserOTP(condition: WhereOptions<any>): Promise<UserOTP | undefined> {
        try {
            const userOTP = await this.userOTPModel.findOne({
                where: condition,
            });
            return userOTP;
        } catch (error) {
            throw error;
        };
    }

}
