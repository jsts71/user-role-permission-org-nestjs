import { Body, Controller, HttpStatus, Post, Get, Req, Res, Put, Param } from '@nestjs/common';
import { Request, Response } from 'express';
import { AllowUsersDTO, UpdateUserDto, UpdateUserEmail, UpdateUserPassword, UpdateUserPhone } from './dto/update-user.dto';
import { CommonService } from 'src/common/common.service';
import { UserService } from './user.service';

@Controller('user')
export class UserController {

    constructor(
        private commonService: CommonService,
        private userService: UserService,
    ) {

    }

    @Post("allow-users-as-org")
    async allowUserAsOrg(@Body() allowUsersDTO: AllowUsersDTO, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName)
        if (!superAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

        let notAllowedOrg = await this.userService.findAll({ id: allowUsersDTO.ids, isAllowed: false, userAsOrg: true });
        let serviceResponse = await this.userService.allowUserAsOrg(notAllowedOrg)
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Post("allow-users-in-org")
    async allowUserInOrg(@Body() allowUsersDTO: AllowUsersDTO, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const orgAdmin = await this.commonService.checkOrganizationAdmin(auth)
        if (!orgAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

        let notAllowedEmployee = await this.userService.findAll({ id: allowUsersDTO.ids, organizationId: auth.userInfo.organizationId, isAllowed: false, userAsOrg: false });

        let serviceResponse = await this.userService.allowUserInOrg(notAllowedEmployee)
        return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Get()
    async fetch(@Req() request: Request, @Res() response: Response) {
        const auth = request.headers.data as any;
        let serviceResponse = await this.userService.fetch(auth.userInfo.id);
        return serviceResponse ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Get(":id")
    async findOne(@Param('id') id: string, @Req() request: Request, @Res() response: Response) {
        const auth = request.headers.data as any;
        let serviceResponse = await this.userService.fetch(id);
        return serviceResponse ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Get("list/all")
    async findAll(@Param('id') id: string, @Req() request: Request, @Res() response: Response) {
        const auth = request.headers.data as any;
        const superAdmin = await this.commonService.checkSuperAdmin(auth.userInfo.userName)
        if (!superAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized");

        let serviceResponse = await this.userService.findAllResponse({});
        return serviceResponse ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Put('update/password')
    async updateUserPassword(@Body() updateUserDto: UpdateUserPassword, @Req() request: Request, @Res() response: Response) {
        const auth = request.headers.data as any;
        let serviceResponse = await this.userService.updateUserPasswordResponse(auth.userInfo.id, updateUserDto);
        return serviceResponse ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Put('update/email')
    async updateUserEmail(@Body() updateUserDto: UpdateUserEmail, @Req() request: Request, @Res() response: Response) {
        const auth = request.headers.data as any;
        let serviceResponse = await this.userService.updateUserEmailResponse(auth.userInfo.id, updateUserDto);
        return serviceResponse ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Put('update/phone')
    async updateUserPhone(@Body() updateUserDto: UpdateUserPhone, @Req() request: Request, @Res() response: Response) {
        const auth = request.headers.data as any;
        let serviceResponse = await this.userService.updateUserPhoneResponse(auth.userInfo.id, updateUserDto);
        return serviceResponse ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Put()
    async update(@Body() updateUserDto: UpdateUserDto, @Req() request: Request, @Res() response: Response) {
        const auth = request.headers.data as any;
        let serviceResponse = await this.userService.updateResponse(auth.userInfo.id, updateUserDto);
        return serviceResponse ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }

    @Put(":id")
    async updateUser(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto, @Req() request: Request, @Res() response: Response) {

        const auth = request.headers.data as any;
        const orgAdmin = await this.commonService.checkOrganizationAdmin(auth)
        if (!orgAdmin) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

        const isExists = await this.userService.fetch(id);
        if(!isExists.success) response.status(HttpStatus.BAD_REQUEST).send("No user")
        if(isExists?.data?.organizationId !== auth.userInfo.organizationId) return response.status(HttpStatus.UNAUTHORIZED).send("Unauthorized")

        let serviceResponse = await this.userService.update({ id, body: updateUserDto as any });
        // return serviceResponse.success ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
        return serviceResponse ? response.send(serviceResponse) : response.status(HttpStatus.BAD_REQUEST).send(serviceResponse);
    }
}
